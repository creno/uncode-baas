# Part I. 显示定义

## 1.displayType 

具体值分类：
1. 值为1，代表txt
2. 值为2，代表自定义，对应displayValue值为key/value对，如：{"1":"是","0":"否"}
3. 值为3，many2one，具体值对应resttable表中的many2one.
4. 值为5，图片

## 2.formType 

具体值分类：
1. 值为1，代表input
2. 值为2，代表select
3. 值为3，代表radio
4. 值为4，代表checkbox
5. 值为5，代表上传组件
5. 值为0，代表不可修改

## 3.searchType 

具体值分类：
1. 值为3，代表equal
2. 值为4，代表not equal
3. 值为5，代表greater than
4. 值为6，代表greater than or equal
5. 值为7，代表less than
6. 值为8，代表less than or equal
7. 值为9，代表like
8. 值为11，代表in
9. 值为0，代表无该字段



------------------------------------------------------------------------

# Part II. 外键定义

## 1. many2one

以key/value方式存储，key代表当前表多方字段，value代表外键表，其中：
1. table代表外键表
2. field代表值字段
3. display代表显示字段
4. order代表排序方式

示例：{"bucket":{"table":"restapp","field":"bucket","display":"name","order":"id"}

## 2. one2many

以key/value方式存储，key代表当前表一方字段，value定义多方外键表及中间表，其中：
1. many代表多方外键表，同many2one
2. join代表中间表，其中：table表名，field当前表id在中间表中的字段名，field2多方表id在中间表中的字段名。

示例：{"id":{"many":{"table":"restfield","field":"id","display":"name","order":"id"},"join":{"table":"method2field","field":"restMethodId","field2":"restFieldId"},"desc":"添加用户"}


--------------------------------------------------------------------------------

# Part III. 部分接口

1 获取当时用户信息
GET /{bucket}/user?username=ywj@xiaocong.tv

2 修改用户信息
POST /{bucket}/user
{"username":"ywj@xiaocong.tv","moblie":"12399887766"}

3 修改密码
POST /{bucket}/resetpass
{"username":"ywj@xiaocong.tv","oldPassword":"1234","password":"32432"}

4 获取所有用户信息
GET /{bucket}/users












