package cn.uncode.baas.server.vo;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.dal.utils.JsonUtils;

public class MethodVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4463521142918762403L;
	
	public static final String NAME = "name";
	public static final String OPTION = "option";
	public static final String VERSION = "version";
	
	//{"name":"restapp","option":"bucket","version":"name"}
	private String name;
	private String option;
	private String version;
	
	public static MethodVo valueOf(String value){
		if(StringUtils.isNotBlank(value)){
			Map<?, ?> tabMap = JsonUtils.fromJson(value, Map.class);
			if(tabMap != null){
				return MethodVo.valueOf(tabMap);
			}
		}
		return null;
	}
	
	public static MethodVo valueOf(Map<?,?> tabMap){
		if(tabMap != null){
			MethodVo vo = new MethodVo();
			if(tabMap.containsKey(NAME)){
				vo.setName(String.valueOf(tabMap.get(NAME)));
			}
			if(tabMap.containsKey(OPTION)){
				vo.setOption(String.valueOf(tabMap.get(OPTION)));
			}
			if(tabMap.containsKey(VERSION)){
				vo.setVersion(String.valueOf(tabMap.get(VERSION)));
			}
			return vo;
		
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	
	

}
