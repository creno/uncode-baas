package cn.uncode.baas.server.vo;

import java.io.Serializable;

public class Page implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1483182560211276285L;
    
    public static int PAGE_SIZE_DEFAULT = 50;
    
    /**
     * 当前页
     */
    private int pageIndex = 1;
    
    /**
     * 每页记录数
     */
    private int pageSize = PAGE_SIZE_DEFAULT;
    
    /**
     * 总页数
     */
    private int pageCount;
    
    /**
     * 总记录数
     */
    private int recordTotal;
    

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        if(pageIndex > 0){
            this.pageIndex = pageIndex;
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if(pageSize > 0){
            this.pageSize = pageSize;
        }
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getRecordTotal() {
        return recordTotal;
    }

    public void setRecordTotal(int recordTotal) {
        if (recordTotal >= 0) {
            this.recordTotal = recordTotal;
        }
        this.pageCount = this.recordTotal / this.pageSize;
        if (this.recordTotal % this.pageSize != 0) {
            this.pageCount++;
        }
        if (this.pageCount == 0) {
            this.pageCount = 1;
        }
        if (this.pageIndex <= 0) {
            this.pageIndex = 1;
        }
        if (this.pageIndex > this.pageCount) {
            this.pageIndex = this.pageCount;
        }
    }

    

}
